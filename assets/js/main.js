let timeline = document.querySelector('#timelines');
let animatable = document.querySelectorAll('.anim8');
const slideTimeline = e => {
    timeline.style.transform = 'translateX(-' + e.target.value / 1.75 + '%)';
};

setTimeout(() => {
    animatable.forEach(el => {
        if (el.getBoundingClientRect().top < 300) {
            el.classList.add('appear')
        } else {
            el.classList.remove('appear')
        }
    });
}, 200);
window.addEventListener('scroll', () => {
    if (window.pageYOffset > 50) {
        document.querySelector('nav').classList.add('scrolled');
    } else {
        document.querySelector('nav').classList.remove('scrolled');
    }

    animatable.forEach(el => {
        if (window.scrollY > el.getBoundingClientRect().top - 500) {
            el.classList.add('appear')
        }
            // else if(window.scrollY< el.getBoundingClientRect().bottom + 200){
            //     el.classList.add('appear')
        // }
        else {
            el.classList.remove('appear')
        }
    })
});

const toggleModal = (val = true) => {
    if (val) {
        document.querySelector('.modal').classList.add('active');
    } else {
        document.querySelector('.modal').classList.remove('active');
    }
};

const toggleNav = (val = true) => {
    if (val) {
        document.querySelector('nav').classList.add('active');
    } else {
        document.querySelector('nav').classList.remove('active');
    }
};